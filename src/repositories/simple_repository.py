import os
import pyodbc

"""
See the Microsoft Tech Community Blog post for inspiration:
https://techcommunity.microsoft.com/t5/apps-on-azure-blog/how-to-connect-azure-sql-database-from-python-function-app-using/ba-p/3035595
"""
class SimpleRepository:
    _driver="{ODBC Driver 17 for SQL Server}"
    _query="SELECT 1 AS TestConnection"
    
    """
    Initialize the repository using the environment variables:
    SERVERNAME: FQDN of the Microsoft SQL Server, e.g. tomato.database.windows.net
    DATABASE: Name of the database to connect to, e.g. tomatodb
    USERNAME: SQL user to login
    PASSWORD: Password of the sql user to login
    """
    def __init__(self, server = None, database = None, username = None, password = None) -> None:
        self._server = server or os.getenv("SERVERNAME")
        self._database = database or os.getenv("DATABASE")
        self._username = username or os.getenv("USERNAME")
        self._password = password or os.getenv("PASSWORD")


    def test_connection(self) -> None:
        connection_string = f'DRIVER={self._driver};SERVER={self._server};DATABASE={self._database};Authentication=ActiveDirectoryMsi'
        self._run_simple_query(connection_string)


    def test_connection_sql_auth(self) -> None:
        connection_string = f'DRIVER={self._driver};SERVER={self._server};DATABASE={self._database};UID={self._username};PWD={self._password}'
        self._run_simple_query(connection_string)


    def _run_simple_query(self, connection_string) -> None:
        conn = pyodbc.connect(connection_string, autocommit=True)
        conn.execute(self._query)


if (__name__ == '__main__'):
    repo = SimpleRepository()
    repo.test_connection()
