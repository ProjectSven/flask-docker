from flask import Flask
import getpass
import platform as pf
import os
from repositories import simple_repository
import logging
from opencensus.ext.azure.log_exporter import AzureEventHandler

app = Flask(__name__)
logger = logging.getLogger(__name__)
# https://portal.azure.com/#@svenlandgrafoutlook.onmicrosoft.com/resource/subscriptions/daf7070d-c86d-4f1f-82b5-0370ca160d33/resourceGroups/rg-autoscale-trigger/providers/microsoft.insights/components/scale-orchestrator/overview
logger.addHandler(AzureEventHandler(connection_string='InstrumentationKey=63379de8-7e71-4a82-b5fb-cd2bdb56c7cc'))
logger.setLevel(logging.INFO)

@app.route("/")
@app.route("/user")
def home():
    properties = { 'custom_dimensions': { 'RunId': '20231026112047077' } }
    logger.info('Get USER info event', extra=properties)
    return f"Hello, my name is {getpass.getuser()}!"


@app.route("/platform")
def platform():
    logger.info('Get PLATFORM info event')
    return f"I'm running on {pf.node()}"


@app.route("/env")
def environment_variables():
    server = os.getenv("SERVERNAME")
    database = os.getenv("DATABASE")
    return f"Servername: '{server}', database: '{database}'"
    

@app.route("/mssql")
def mssql():
    try:
        repo = simple_repository.SimpleRepository()
        repo.test_connection()
        return "Test OK"
    except Exception as ex:
        return str(ex)

