# Simple-Flask

This application is used to demonstrate the use of System Managed Identity in Azure App Service with a Python docker image running on it. To reproduce this setup, follow these steps:

1. Make sure you have an Azure SQL Server with Azure SQL Database available. The Azure SQL Server should allow Azure Active Directory authentication (to be certain, you could disable SQL authentication)
2. Create an Azure App Service for hosting the Linux docker container. Enable an Identity, either System assigned or User assigned. When using System assigned, take note of the name of the Azure App Service, this will be the username needed to grant access to Azure SQL Server
3. Add the Azure App Service as a user to the Azure SQL Database and give it the `db_datareader` role, by running the following statements as an Azure AD user:
  ```
  CREATE USER YourAzureAppService FROM EXTERNAL PROVIDER;
  ALTER ROLE db_datareader ADD MEMBER YourAzureAppService
  ```
4. Enable the Azure SQL Server firewall and allow traffic from other Azure Resources
5. Deploy this Docker image to the Azure App Service
6. Set the following Application Settings:
    - `WEBSITES_PORT` = 5000 (This instructs the Azure App Service to expose the Docker port)
    - `SERVERNAME` = YourAzureSqlServer.database.windows.net
    - `DATABASE` = YourAzureSqlDatabase


Now use your favorite browser to check all the nice endpoints:


# Available endpoints
This minor app has the following endpoints to use:

- `/`
- `/user`
  
  Display the current username (from within Docker)

- `/platform`

  Display the current hostname (from within Docker)

- `/env`

  Display the SERVERNAME and DATABASE Environment values

- `/mssql`
  
  Run a simple query to the configured MS SQL Database*, as the current user. SERVERNAME and DATABASE should be given or set as Environment variables (via AppSettings)


# Running locally in Docker

To run this simple Flash app in Docker:

1. Build the container image. From the root of the project, run:
```
docker build -t flask_docker .
```
2. Start the container, and expose the tcp:5000 port, run:
```
docker run -d --name simpleflask -e SERVERNAME=servername -e DATABASE=database -p 5000:5000 flask_docker
```
3. Access this great app on http://localhost:5000


# Pushing to registry

